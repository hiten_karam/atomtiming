'use strict';

var gulp = require('gulp'),
gutil = require('gulp-util'),
webserver = require('gulp-webserver'),
concat = require('gulp-concat'),
browserify = require('gulp-browserify'),
gulpif = require('gulp-if'),
uglify = require('gulp-uglify'),
minify = require('gulp-minify');
// firebaseref = require('firebase');
var Templation  = require('nodemailer-templation');
var path        = require('path');
//Create our new new mailer object
var Mailer = new Templation({
    from: 'Welcome from Atom Timing',
    templates: {
     reply: path.resolve(__dirname, 'builds/angularregistration/templates/welcome.html')
   },
    transportOptions: {
      host: 'smtp.gmail.com',
      auth: {
        user: 'atomtiming@gmail.com',
        pass: 'NewAtom@123'
      }
    }
  });

  var Mailer2 = new Templation({
      from: 'Payment details from Atom Timing',
      templates: {
       reply: path.resolve(__dirname, 'builds/angularregistration/templates/payment.html')
     },
      transportOptions: {
        host: 'smtp.gmail.com',
        auth: {
          user: 'atomtiming@gmail.com',
          pass: 'NewAtom@123'
        }
      }
    });

var config = {
  apiKey: "AIzaSyBFlBs2uBYC6ugkNmYKVWlrcy2IMfxTOMM",
  authDomain: "enduro-e6464.firebaseapp.com",
  databaseURL: "https://enduro-e6464.firebaseio.com",
  projectId: "enduro-e6464",
  storageBucket: "enduro-e6464.appspot.com",
  messagingSenderId: "993299276744"
};

// var firebase = new firebaseref("https://enduro-e6464.firebaseio.com/enduro-e6464");

// var firebase = firebaseref.initializeApp(config);
// var database = firebase.database();
// var ref = firebase.database().ref();
// var regRef = ref.child('eventsdump');


var env, jsSources, htmlSources, cssSources, outputDir;

env = process.env.NODE_ENV || 'development';

if(env==='development'){
outputDir = 'builds/angularregistration';
}else {
  outputDir = 'builds/angularregistration/prod';
}

var jsSources = [
  // 'builds/angularregistration/js/lib/angular.min.js',
  // 'builds/angularregistration/js/lib/angular-route.min.js',
  // 'builds/angularregistration/js/lib/angular/angular-animate.min.js',
  // 'builds/angularregistration/js/lib/angular-material.min.js',
  'builds/angularregistration/js/app.js',
  'builds/angularregistration/js/lib/ocLazyLoad.js',
  'builds/angularregistration/js/ng-map.min.js',
  'builds/angularregistration/js/services/authentication.js',
  'builds/angularregistration/js/controllers/registration.js',
  'builds/angularregistration/js/controllers/maps.js',
  'builds/angularregistration/js/controllers/events.js',
  'builds/angularregistration/js/controllers/contactus.js',
  'builds/angularregistration/js/controllers/viewEvents.js',
  'builds/angularregistration/js/controllers/profileController.js',
  'builds/angularregistration/js/lib/braintree.js',
  'builds/angularregistration/js/controllers/braintreecheckout.js'

];

// Braintree express starts
var util = require('util'),
express = require('express'),
braintree = require('braintree'),
bodyParser = require('body-parser');

/**
 * Instantiate your server and a JSON parser to parse all incoming requests
 */
var app = express(),
jsonParser = bodyParser.json();
/**
 * Instantiate your gateway (update here with your Braintree API Keys)
 */
var gateway = braintree.connect({
    environment:  braintree.Environment.Sandbox,
    merchantId:   'sk5xdgh3wdrp84s8',
    publicKey:    'yqq6c2b8fmjksjch',
    privateKey:   '1d104fe7062fbe104d8830de7d933943'
});
/**
 * Enable CORS (http://enable-cors.org/server_expressjs.html)
 * to allow different clients to request data from your server
 */
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.use(express.static('builds/angularregistration'));
/**
 * Route that returns a token to be used on the client side to tokenize payment details
 */

 app.post('/api/v1/welcomeEmail',jsonParser, function (request, response) {
   // console.log(request.body);
   console.log('Welcome email to ' + request.body.userdata.email);
   console.log(request.body.userdata.firstname + ' ' + request.body.userdata.lastname);
   Mailer.send({
     to: request.body.userdata.email,
     subject: 'Welcome to Atom Timing',
     template: 'reply',
     messageData: {
       customerName: request.body.userdata.firstname + ' ' + request.body.userdata.lastname
     }
   });
     response.json({
       "client_token": 'success'
     });
   });

app.post('/api/v1/token', function (request, response) {
  gateway.clientToken.generate({}, function (err, res) {
    if (err) throw err;
    response.json({
      "client_token": res.clientToken
    });
  });
});
/**
 * Route to process a sale transaction
 */
app.post('/api/v1/process', jsonParser, function (request, response) {
  var transaction = request.body;
  gateway.transaction.sale({
    amount: transaction.amount,
    customer: {
      firstName: transaction.user.firstname,
      lastName: transaction.user.lastname,
      email: transaction.user.email

    },
    customFields: {
    event: transaction.eventpart2,
    eventids: transaction.eventpart1
},
    paymentMethodNonce: transaction.payment_method_nonce,
    options:{
      submitForSettlement: true
    }
  }, function (err, result) {
    var transdate = new Date(result.transaction.createdAt.toString());
    transdate = transdate.getDate()+'-' + (transdate.getMonth()+1)+'-'+ transdate.getFullYear();
    var eventsparticipated = result.transaction.customFields.event.toString();
    eventsparticipated = eventsparticipated.substring(2,eventsparticipated.length);
    eventsparticipated = eventsparticipated.split("&&").toString();
    console.log(result.transaction.customer.email);
    console.log(result.transaction.amount);
    // console.log('result.customFields.event ' + eventsparticipated.substring((eventsparticipated.indexOf(",")+1),eventsparticipated.length));
    if (err) throw err;
    console.log('transaction result '+ util.inspect(result));
    if(result.success)
    {
    Mailer2.send({
      to: result.transaction.customer.email,
      subject: 'Event Participation Fee',
      template: 'reply',
      messageData: {
        Amount: result.transaction.amount,
        participant: result.transaction.customer.firstName + ' ' + result.transaction.customer.lastName,
        Events: eventsparticipated,
        TransID: result.transaction.id,
        Transdate: transdate
      }
    });
  }
    response.json(result);
  });
});

app.get('/api/v1/tokenf', function (request, response) {
   response.json(config);
});

app.listen(8000,function () {
  console.log('Listening on port 8000');
});

// Braintree express ends

gulp.task('js', function() {
  console.log(jsSources);
  gulp.src(jsSources)
  .pipe(concat('angular.js'))
  .pipe(uglify())
  .pipe(gulp.dest('builds/angularregistration/js/lib/angular'));
  console.log('angular.js done');
});

gulp.task('html', function() {
  gulp.src('builds/angularregistration/*.html');
});

gulp.task('css', function() {
  gulp.src('builds/angularregistration/css/*.css');
});

gulp.task('watch', function() {
  gulp.watch('builds/angularregistration/js/**/*', ['js']);
  gulp.watch('builds/angularregistration/css/*.css', ['css']);
  gulp.watch(['builds/angularregistration/*.html',
    'builds/angularregistration/views/*.html'], ['html']);
});

gulp.task('webserver', function() {
  gulp.src('builds/angularregistration/')
    .pipe(webserver({
      // livereload: true,
      host: '0.0.0.0',
      open: true
    }));
});

gulp.task('default', ['watch', 'html', 'js', 'css', 'webserver']);
