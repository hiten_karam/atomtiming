myApp.factory("Authentication", ["$rootScope", "$location", "$timeout", "$firebaseObject", "$firebaseAuth", "$http", function(e, t, n, a, i, s) {
  var r, d = firebase.database().ref(),
    o = i();
  return o.$onAuthStateChanged(function(t) {
    if (t) {
      var n = d.child("users").child(t.uid),
        i = a(n);
      e.currentUser = i
    } else e.currentUser = ""
  }), r = {
    welcome: function(e) {
      s({
        method: "POST",
        url: "/api/v1/welcomeEmail",
        data: {
          userdata: e
        }
      }).then(function(e) {}).catch(function(e) {})
    },
    login: function(n) {
      e.message = "", $("#spinner_icon").show(), $("#hide_text").hide(), angular.element(document.getElementById("fieldsetform"))[0].disabled = !0, o.$signInWithEmailAndPassword(n.email, n.password).then(function(e) {
        t.path("/events")
      }).catch(function(t) {
        e.message = t.message, angular.element(document.getElementById("fieldsetform"))[0].disabled = !1, $("#spinner_icon").hide(), $("#hide_text").show()
      })
    },
    logout: function() {
      return sessionStorage.clear(), e.message = "", o.$signOut()
    },
    requireAuth: function() {
      return o.$requireSignIn()
    },
    register: function(t) {
      $("#spinner_icon").show(), $("#hide_text").hide(), e.message = "", angular.element(document.getElementById("fieldsetform"))[0].disabled = !0;
      var n = "ND";
      n = document.getElementById("Male").checked ? "Male" : document.getElementById("Female").checked ? "Female" : document.getElementById("Other").checked ? "Other" : "ND", o.$createUserWithEmailAndPassword(t.email, t.password).then(function(e) {
        var a = new Date(t.bday);
        d.child("users").child(e.uid).set({
          date: firebase.database.ServerValue.TIMESTAMP,
          dob: a.getDate() + "-" + (a.getMonth() + 1) + "-" + a.getFullYear(),
          regUser: e.uid,
          firstname: t.firstname,
          lastname: t.lastname,
          email: t.email,
          gender: n
        });
        r.welcome(t), r.login(t)
      }).catch(function(t) {
        e.message = t.message, angular.element(document.getElementById("fieldsetform"))[0].disabled = !1, $("#spinner_icon").hide(), $("#hide_text").show()
      })
    }
  }
}]);
